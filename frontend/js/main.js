const host = "localhost"

let session_id = null;
let username = null;


// Notifications
const Borders = {
    warning : "border-warning",
    success : "border-success"
}

const States = {
    no_session : 0,
    logged : 1
}

const Visibilities = {
    true : "visible",
    false : "hidden"
}

let state = States.no_session;

let active_notifications = 0;
const notification_height = 30;


//Utilities
function getCookie(name) {
    function escape(s) { return s.replace(/([.*+?\^$(){}|\[\]\/\\])/g, '\\$1'); }
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : null;
}

function changeElementClass(element, from, to)
{
    if (!element || !from || !to)
        console.error(`Expected 3 arguments, got ${element}, ${from} and ${to}`);
    let classes = element.className;
    element.className = classes.replace(new RegExp(`(?:^|\s)${from}(?!\S)`, 'g'), to);
}

/*
    Problemas: 
    O valor da property pode passar ou não chegar no valor desejado.
    seems scuffed
    
    @params
    time = time in ms
    fps = frames per second
*/
function lerp_css_property(element, property, unit, from, to, time, fps){
    var runningTime = 0;
    var period = 1000/fps;
    var frames = time / period;
    var change = (to - from)/frames;
    var value = from;
    var timer = setInterval(() => {
        if (!element )
            return clearInterval(timer);
        value += change;
        element.style.setProperty(property, `${value}${unit}`);
        runningTime += period;
        if (runningTime >= time)
            return clearInterval(timer);
        
    }, period);
}

function delete_element_after(element, time, callback)
{
    if (!element || !time)
        return console.error(`Expected 2 parameters and got ${element} and ${time}`);
    var timer = setTimeout(() =>  {
        if (element){
            clearTimeout(timer);
            element.parentNode.removeChild(element);
            callback();
        }
    }, time);
}

function show_notification(text, border){
    var container = document.createElement('DIV');
    container.className = "notification-container center-content";
    var notification = document.createElement('DIV');
    notification.className = `center-content notification ${border}`;
    container.appendChild(notification);
    document.body.appendChild(container);
    notification.innerHTML = text;
    active_notifications += 1;
    lerp_css_property(container,"top", "px", 0, active_notifications * notification_height, 100, 60);
    lerp_css_property(container,"opacity", "", 0, 0.9, 300, 60);
    delete_element_after(container, 5000, () => {
        active_notifications -= 1;
    });

}


function request_register()
{
    const username = document.querySelector("#register-username").value;
    const email = document.querySelector("#register-email").value;
    const password = document.querySelector("#register-password").value;

    const request = new XMLHttpRequest();

    request.open("POST", "/user/", true);
    request.setRequestHeader("username", username);
    request.setRequestHeader("email", email);
    request.setRequestHeader("password", password);
    request.onload = function (e) {
    if (request.readyState === 4) {
        if (request.status === 200) {
            show_notification("Created account successfuly!", Borders.success);
        } else {
            show_notification(request.responseText, Borders.warning);
        }
    }
    };
    request.send(null); 
}

function request_login()
{
    const email = document.querySelector("#login-email").value;
    const password = document.querySelector("#login-password").value;

    const request = new XMLHttpRequest();

    request.open("GET", "/user/login", true);
    request.setRequestHeader("email", email);
    request.setRequestHeader("password", password);
    request.onload = function (e) {
    if (request.readyState === 4) {
        if (request.status === 200) {
            show_notification("Logged in sucessfully!", Borders.success);
            session_id = request.getResponseHeader("session_id");
            state = States.logged;
            refresh_data();
            document.querySelector("#login").style.setProperty("visibility", "hidden");
            document.querySelector("#register").style.setProperty("visibility", "hidden");
        } else {   
            show_notification(request.responseText, Borders.warning);
        }
    }
    };
    request.send(null); 
}

let login_visibility = false;
let register_visibility = false;

function click_register(){
    login_visibility = false;
    register_visibility = true;
    document.querySelector("#login").style.setProperty("visibility", Visibilities[login_visibility]);
    document.querySelector("#register").style.setProperty("visibility", Visibilities[register_visibility]);
}

function click_login(){
    register_visibility = false;
    login_visibility = true;
    document.querySelector("#login").style.setProperty("visibility", Visibilities[login_visibility]);
    document.querySelector("#register").style.setProperty("visibility", Visibilities[register_visibility]);
}

function click_profile(){
    switch (state){
        case States.no_session:
            login_visibility = !login_visibility;
            document.querySelector("#login").style.setProperty("visibility", Visibilities[login_visibility]);
            break;
        case States.logged:
            show_notification("Not implemented :c", Borders.warning);
            break;
        default:
            break;
    }
}

function refresh_data(){
    const profile = document.querySelector("#profile");
    if (!username)  {
        request_data().then(
            (res) => {
                profile.innerHTML = username;
            },
            (err) => {
                return;
            }
        )
    }
}

function request_data(){
    return new Promise((resolve, reject) => {
        if (!session_id)
        {
            show_notification("Log in required!", Borders.warning);
            state = States.no_session;
            return reject();
        }
        const request = new XMLHttpRequest();
    
        request.open("GET", "/user/", true);
        request.setRequestHeader("session_id", session_id);
        request.onload = function (e) {
        if (request.readyState === 4) {
            if (request.status === 200) {
                username = request.getResponseHeader("username");
                return resolve(username);
            } else {   
                console.error(request.responseText);
                return reject(request.responseText);
            }
        }
        };
        request.send(null);
    });
}

document.addEventListener("DOMContentLoaded", function(){
    document.querySelector("#register-submit").addEventListener("click", request_register);
    document.querySelector("#login-submit").addEventListener("click", request_login);
    document.querySelector("#profile").addEventListener("click", click_profile);
    document.querySelector("#register-button").addEventListener("click", click_register);
    document.querySelector("#login-button").addEventListener("click", click_login);
});