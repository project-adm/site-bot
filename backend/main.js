const fs = require('fs').promises;

const Servers = {};

const servers_path = "./servers/";

function Initialize()
{
    const servers = fs.readdir(servers_path, {withFileTypes: true});

    servers.then((files) => {
        for (const file of files){
            if (file.isDirectory())
                continue;
            const name = file.name.replace(/\.\D+/, ""); //Remove file extension
            try{
                Servers[name] = require(servers_path + file.name);
                Servers[name].initialize();
                console.log(`Initialized server [${name}]`);
            }catch(err){
                console.log(`Failed to load ${name} with error ${err.stack}`);
            }   
        }
    }, (err) => 
    {
        console.log(`Errored: ${err}`);
    });
}

Initialize();

