const http = require('http');
const url = require('url');
const util = require('util');
const fs = require('fs').promises;

// global.Reponses = //
global.Responses = require('./modules/responses');
global.Mongo = require('./modules/mongodb');
global.Sessions = require("./modules/sessions");


const endpoints_path = "./servers/endpoints/";

/* 
    Endpoint Object
    method - POST
    method - GET
    method - PUT
    method - PATCH
    method - DELETE

    class Endpoint
    {
        
    not_implemented(res){
        res.writeHead(501)
        res.end("Not implemented");
    }

    post(req, res, url) { this.not_implemented(res); };
    get(req, res, url) { this.not_implemented(res); };
    put(req, res, url) { this.not_implemented(res); };
    patch(req, res, url) { this.not_implemented(res); };
    delete(req, res, url) { this.not_implemented(res); };
}
    */


const Server = {
    hostname : "0.0.0.0",
    port : 9900,
    endpoints : {},
    site_path : "../frontend/",
    handleRequests : function(self, request, response)
    {   
        const url = new URL(request.url, `http://${request.headers.host}`);
        
        let endpoint = self.endpoints[url.pathname];
        if (!endpoint)
        {
            let name = /.+(?=\/)/g.exec(url.pathname);
            if (name)
                endpoint = self.endpoints[name];
        }
    
        // /(?<=\/)[^\/]+/g Matches anything that is not a '/' that is after a '/'

        if (endpoint){
            switch(request.method)
            {
                case "POST":
                    endpoint.post(request, response, url);
                    break;
                case "GET":
                    endpoint.get(request, response, url);
                    break;
                case "PUT":
                    endpoint.put(request, response, url);
                    break;
                case "PATCH":
                    endpoint.patch(request, response, url);
                    break;
                case "DELETE":
                    endpoint.delete(request, response, url);
                    break;
                default:
                    response.writeHead(501)
                    response.end("METHOD not implemented.");
                    break;
            }
        }
        else
        {
            if (request.url == "/") // There ought to be a better solution than this
            {
                request.url = "/home.html";
            }
        
            let file = fs.readFile(self.site_path + request.url);
        
            file.then(
                (file) => {
                    response.writeHead(200),
                    response.end(file);
                },
                (err) => {
                    response.writeHead(404);
                    response.end("RESOURCE NOT FOUND!");
                    console.log(err);
                }
            )
        }
    },
    initialize : function()
    {

        this.server = http.createServer((req, res) => {this.handleRequests(this, req, res)});
        this.server.listen(this.port, this.hostname);
    
        const actual_path = "./endpoints/";

        const server = this;
    
        const endpoints_from_dir = function(dir, relative="/"){
            const path = actual_path + relative;
            const endpoints = fs.readdir(dir + relative, {withFileTypes: true});
            endpoints.then((files) => {
                for (const file of files){
    
                    if (file.isDirectory())
                    {
                        if (relative != "/")
                            endpoints_from_dir(dir, `${relative}${file.name}/`);
                        else
                            endpoints_from_dir(dir, `/${file.name}/`);
                        continue;
                    }
    
                    const name = relative + file.name.replace(/\.\D+/, ""); //Remove file extension
                        
                    try{
                        server.endpoints[name] = require(path + file.name);
                        console.log(`Added endpoint [${name}]`);
                    }catch(err){
                        console.log(`Failed to load endpoint ${name} with error ${err.stack}`);
                    }   
                }
            }, (err) => 
            {
                console.log(`Errored: ${err}`);
            });
        }
    
        endpoints_from_dir(endpoints_path);
    }

}

module.exports = Server;