const argon2 = require('argon2');
const Responses = require('../../modules/responses');
const Sessions = require('../../modules/sessions');

const Endpoint = {
    hashVersion : 1
}

Endpoint.not_implemented = function (res){
    res.writeHead(501)
    res.end("Not implemented");
}

Endpoint.get = async function (incoming, outgoing, url) {
    const password = incoming.headers.password;
    const email = incoming.headers.email;
    try {
        if (!password || !email)
            return Responses.respond(outgoing, Responses.codes.bad_request, "Missing one of the following headers. 'password' or 'email'. ")
        else
        {
            Mongo.checkEmail(email).then(
                async response => {
                    if (response){        
                        // Verify e-mail but for now we just create the account        
                        Mongo.fetchUserByEmail(email).then(
                            async user => {
                                if (!user.name || !user.email || !user.hash || !user.hashVersion) 
                                    throw new Error("Missing name, email, hash or hashversion");
                                argon2.verify(user.hash, password).then(
                                    success => {
                                        if (success)
                                            {
                                                const session_id = Sessions.createSession(user._id)
                                                return Responses.respond(outgoing, Responses.codes.ok, "Ok.", {"session_id": session_id});
                                            }
                                        else
                                            return Responses.respond(outgoing, Responses.codes.unauthorized, "Invalid credentials", {invalid_credentials : true});
                                    },
                                    err => {throw err}
                                )
                            },
                            err => {
                                throw err;
                            }
                        )
                    }
                    else{
                        return Responses.respond(outgoing, Responses.codes.bad_request, "Email is not registered", {email_valid : false});
                    }
                },
                err => {throw err}
            );
        }
    }catch(err){
        console.log(`[Failed to create user](Hashing) @ timestamp : {${err}}`)
        return Responses.respond(outgoing, Responses.codes.internal_error, "Internal server error")
    }};

Endpoint.post = async function (incoming, outgoing, url) { this.not_implemented(outgoing);};
Endpoint.put = function (incoming, outgoing, url) { this.not_implemented(outgoing); };
Endpoint.patch = function (incoming, outgoing, url) { this.not_implemented(outgoing); };
Endpoint.delete = function (incoming, outgoing, url) { this.not_implemented(outgoing); };

module.exports = Endpoint;