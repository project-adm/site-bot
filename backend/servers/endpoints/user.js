const argon2 = require('argon2');
const Mongo = require('../modules/mongodb');
const Responses = require('../modules/responses');


const Endpoint = {
    hashVersion : 1
}

Endpoint.not_implemented = function (res){
    res.writeHead(404)
    res.end("Not found");
}
/*
    Problemas: Não há requerimentos para nome, e-mail e senha, fora a de que o e-mail não pode estar cadastrado.
*/
Endpoint.post = async function (incoming, outgoing, url) {
    const username = incoming.headers.username;
    const password = incoming.headers.password;
    const email = incoming.headers.email;
    try {
        if (!username || !password || !email)
            return Responses.respond(outgoing, Responses.codes.bad_request, "Missing one of the following headers. 'username' or 'password' or 'email'. ")
        else
        {
            Mongo.checkEmail(email).then(
                async res => {
                    if (!res){
                        const hash = await argon2.hash(password, {
                            type : argon2.argon2id,
                            memoryCost : 37000,
                            timeCost : 2
                        });
        
                        // Verify e-mail but for now we just create the account        
                        Mongo.createUser(username, email, hash, this.hashVersion).then(
                            res => {
                                return Responses.respond(outgoing, Responses.codes.ok, "ok.");
                            },
                            err => {
                                throw err;
                            }
                        )
                    }
                    else{
                        return Responses.respond(outgoing, Responses.codes.bad_request, "Email is already registered", {invalid_email : true});
                    }
                },
                err => {throw err}
            );
        }
    }catch(err){
        console.log(`[Failed to create user](Hashing) @ timestamp : {${err}}`)
        return Responses.respond(outgoing, Responses.codes.internal_error, "Internal server error")
    }
};
Endpoint.get = function (incoming, outgoing, url) {
    const session = incoming.headers.session_id;
    try {
        if (!session)
            return Responses.respond(outgoing, Responses.codes.bad_request, "Missing one of the following headers. 'session_id'. ")
        else
        {
            const o_session = Sessions.getSession(session);
            if (!o_session)
                return Responses.respond(outgoing, Responses.codes.bad_request, "Invalid session.", {invalid_session : true});
            Mongo.fetchUserByID(o_session.user).then(
                (res) => {
                    if (res)
                        return Responses.respond(outgoing, Responses.codes.ok, "Ok.", {username : res.name});
                    else
                        return Responses.respond(outgoing, Responses.codes.bad_request, "Could not fetch user data.");
                },
                (err) => {
                    throw err;
                }
            )
        }
    }catch(err){
        console.log(`[Failed to fetch user](Fetching) @ timestamp : {${err}}`)
        return Responses.respond(outgoing, Responses.codes.internal_error, "Internal server error")
    }
};
Endpoint.put = function (incoming, outgoing, url) { this.not_implemented(outgoing); };
Endpoint.patch = function (incoming, outgoing, url) { this.not_implemented(outgoing); };
Endpoint.delete = function (incoming, outgoing, url) { this.not_implemented(outgoing); };

module.exports = Endpoint;