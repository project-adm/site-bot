const crypto = require('crypto')

/*
    Sessões não são únicas por usuário, cada usuário pode ter mais de uma sessão ativa.
*/

const Sessions = {
    active_sessions : {},
    session_timeout : 1 * 60 * 60 * 1000
}

class Session{
    constructor(userID){
        this.user = userID;
        this.timestamp = Date.now() + Sessions.session_timeout;
    }

    is_valid(){
        return Date.now() >= this.timestamp;
    }
}

Sessions.createSession = function(userID){
    let session_id = null; //May cause bugs
    do
        session_id = crypto.randomBytes(16).toString('Base64');
    while (this.active_sessions[session_id])     

    this.active_sessions[session_id] = new Session(userID);
    return session_id;
}

Sessions.getSession = function(session_id){
    return this.active_sessions[session_id];
}


module.exports = Sessions;