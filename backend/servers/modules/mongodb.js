const mongoose = require('mongoose');

function connect()
{
    mongoose.connect("mongodb://localhost:27017/site-bot", {useNewUrlParser: true, useUnifiedTopology: true}).catch(
        err => connect()
    );
}

connect();

const Mongo = {
    ready : false,
    db : mongoose.connection,
}

Mongo.db.on('error', function(err) {
    console.log(`Error ${err}`);
    }
)

Mongo.db.on('disconnected', 
    err =>   {
        Mongo.ready = false;
    }
)

Mongo.db.once('open', function() {
    Mongo.ready  = true;
});

const USER_SCHEMA = new mongoose.Schema({
    name : String,
    email : String,
    hash : String,
    hashVersion : Number
});

const USER = mongoose.model("USER", USER_SCHEMA);

Mongo.checkEmail = function(user_email){
    return USER.exists({email : user_email});
}

Mongo.fetchUserByEmail = function(user_email){
    return USER.findOne({email : user_email});  //Here we make the assumption that e-mails are unique
}

Mongo.fetchUserByID = function(user_id){
    return USER.findOne({_id : user_id});
}

Mongo.userCount = function()
{
    return new Promise((resolve, reject) => {
        const userCount = USER.estimatedDocumentCount({}, () => {
            if (userCount)
                resolve(userCount);
            else
                reject(userCount);
        });
    });
}

Mongo.createUser = function(name, email, hash, hashversion)
{
    return new Promise((resolve, reject) => {
        this.userCount().then(
            (count) => {
                if (count && count <= 1000)
                {
                    USER.create({name: name, email: email, hash: hash, hashVersion: hashversion}).then(
                        (user) => {
                            resolve(user);
                        },
                        (err) => {
                            reject(err);
                        }
                    )
                }
            },
            (err) => {
                reject(err);
            }
        )
    })  
}

module.exports = Mongo;