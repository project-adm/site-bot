const Responses = {
    codes : {
        ok : 200,
        bad_request : 400,
        unauthorized : 401,
        too_many_requests : 429,
        internal_error : 500
    }
}

Responses.respond = function(res, code, message, headers = {}){
    res.writeHead(code, headers);
    res.end(message);
}

module.exports = Responses;